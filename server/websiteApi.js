import { createRequire } from 'module';
const require = createRequire(import.meta.url);


const express = require('express')
const cors = require('cors');
const mysql = require('mysql');

const app = express();
const port = 8000;


// Configuring database
const db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "spawk"
  });

db.connect((err) => {
    if (err) console.log("Database Connection Failed !!!", err);
    else console.log("Connected to Database");
});
//


// Configuring body parser middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));



app.post('/register', (req, res) => {
    const { login, email, password } = req.body;

    let query = `INSERT INTO users (login, email, password) VALUES (?, ?, ?);`;

    db.query(query, [login, email, password], (err, rows) => {
        if (err) {
            res.status(500);
            res.json(err);
        } else {
            const data = { userId: rows.insertId };
            res.json(data);
        }
    });
});

app.get('/login/:login/:password', (req, res) => {
    const { login, password } = req.params;

    let query = `SELECT * FROM users WHERE login = ? AND password = ?`;

    db.query(query, [login, password], (err, rows) => {
        if (err) {
            res.status(500);
            res.json(err);
        } else {
            if(rows.length > 0) {
                const data = { userId: rows[0].id };
                res.json(data);
            } else {
                res.json(["error"]);
            }
        }
    });
});

app.put('/edit/:userId/:column/:value', (req, res) => {
    const { userId, column, value } = req.params;

    let query = `UPDATE users SET ?? = ? WHERE id = ?`;

    db.query(query, [column, value, userId], (err, rows) => {
        if (err) {
            res.status(500);
            res.json(err);
        } else {
            let data = { affected: rows.affectedRows};
            res.json(data);
        }
    });
});


app.get('/user/:userId', (req, res) => {

    const { userId } = req.params; 

    if(!(userId % 1 === 0)) {
        res.status(500);
        res.json(["Aucun id n'est passé en paramètre."]);
        return;
    } 

    let query = `SELECT * FROM users WHERE id = ?`;

    db.query(query, [userId], (err, rows) => {
        if (err) {
            res.status(500);
            res.json(err);
            return;
        } else {
            res.json(rows);
        }

    });
});

app.get('/ranking', (req, res) => {

    let query = `SELECT * FROM users ORDER by score`;

    db.query(query, (err, rows) => {
        if (err) {
            res.status(500);
            res.json(err);
            return;
        } else {
            res.json(rows);
        }

    });
});

app.listen(port, () => console.log(`Spawk.io WebsiteApi listening on port : ${port}!`));