import { createRequire } from 'module';
const require = createRequire(import.meta.url);

const express = require('express')();
const http = require('http').Server(express);
const io = require('socket.io')(http);
const axios = require('axios');

import {Game} from './class/Game.js';

var currentGame = new Game(io);

http.listen(3000, () => {
  console.log('Listening on port *: 3000');
});
