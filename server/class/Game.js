// try {
//     await axios.put(
//       http://localhost:8000/edit/${userId}/${element.key}/${element.value}
//     );
//   } catch (e) {
//     console.log(e);
//   }

import {Player} from './Player.js';
import { Room } from './Room.js';

var socketInterval = [];

const canvas = {
    'height': 500,
    'width': 800, 
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

class Game {
    constructor(io) {
      this.players = [];
      this.rooms = [];
      this.io = io;
      this.run();
    }

    createRoom(roomName, state) {
        if(!this.roomExist(roomName)) {
            let room = new Room(roomName, state);
            this.rooms.push(room);
        }
    }

    async roomInfo(roomName) {
        let roomIndex = this.rooms.findIndex((element) => element.name === roomName);
        let players = await this.getRoomPlayers(roomName);

        if(roomIndex !== -1) return [true, roomIndex, players];
        else return [false, roomIndex, players];
    }

    roomExist(roomName) {
        let roomIndex = this.rooms.findIndex((element) => element.name === roomName);

        if(roomIndex !== -1) return true;
        else return false;
    }

    getRoomPlayers(roomName) {
        return new Promise(resolve => {
            this.io.of('/').in(roomName).clients((error,clients) => {
                resolve(clients);
            });
        });
    }

    createPlayer(socket, name, roomName) {
        let playerExist = this.playerExist(socket); 
        if(!playerExist[0]) {
            let player = new Player(socket, name, roomName);
            this.players.push(player);
        }
    }

    playerExist(socket) {
        let playerIndex = this.players.findIndex((element) => element.socket === socket);

        if(playerIndex !== -1) return [true, playerIndex];
        else return [false, playerIndex];
    }

    handleBonus(playerExist) {
        let bonus = this.players[playerExist[1]].bonus;
        if(bonus === null) return;

        if(bonus === 0) {
            if(this.players[playerExist[1]].isHunter) this.players[playerExist[1]].radius = 30;
            else this.players[playerExist[1]].radius = 7.5;
        }
        else if(bonus === 1) {
            this.players[playerExist[1]].speed = 1.5;
        }
        this.players[playerExist[1]].bonus = null;
        setTimeout(() => {
            this.players[playerExist[1]].radius = 15;
            this.players[playerExist[1]].speed = 1;
        }, 5000);
    }

    handleRoom(roomName, auto = false) {
        this.roomInfo(roomName).then(roomInfo => {
            if(roomInfo[0]) {
                if(!auto) {
                    let huntedCount = 0;
                    let hunterCount = 0;
                    let isSafe = 0;
                    this.players.forEach(element => {
                        if(element.roomName === roomName) {
                            if(!element.isHunter) huntedCount++;
                            else hunterCount++;

                            if(!element.isHunter && element.isSafe) isSafe++;
                        }
                    });

                    if((huntedCount === 0 || hunterCount === 0) && this.rooms[roomInfo[1]].state) {   
                        this.rooms[roomInfo[1]].state = false;
                        this.players.forEach(element => {
                            if(element.roomName === roomName) {
                                element.isSafe = true;
                            }
                        });
                        clearInterval(this.rooms[roomInfo[1]].endingTimer)
                        setTimeout(() => {
                            if(roomInfo[2].length >= 5) {
                                this.rooms[roomInfo[1]].state = true;
                                this.rooms[roomInfo[1]].round = 1;
                                this.rooms[roomInfo[1]].remainingTime = 60;
                                this.players.forEach(element => {
                                    if(element.roomName === roomName) {
                                        element.posX = 30;
                                        element.posY = canvas.height / 2;
                                        element.isSafe = false;
                                        element.isHunter = false;
                                    }
                                });
                                let bingo = getRandomInt(roomInfo[2].length);
                                let roomPlayers = [];
                                this.players.forEach(element => {
                                    if(element.roomName === this.rooms[roomInfo[1]].name) roomPlayers.push(element);
                                });
                                roomPlayers[bingo].isHunter = true;
                                roomPlayers[bingo].posX = canvas.width / 2;
                                this.rooms[roomInfo[1]].endingTimer = setInterval(() => {
                                    if(typeof this.rooms[roomInfo[1]] != 'undefined' && this.rooms[roomInfo[1]].remainingTime !== null) {
                                        if(this.rooms[roomInfo[1]].remainingTime > 0) {
                                            this.rooms[roomInfo[1]].remainingTime--;
                                        } else {
                                            this.rooms[roomInfo[1]].remainingTime = 60;
                                            this.handleRoom(this.rooms[roomInfo[1]].name, true);
                                        }
                                    }
                                }, 1000);
                            }
                        }, 10000);
                        console.log('Partie terminé')
                    }

                    if(this.rooms[roomInfo[1]].round === 10) {
                        if(isSafe === huntedCount && this.rooms[roomInfo[1]].state) {
                            this.rooms[roomInfo[1]].state = false;
                            this.players.forEach(element => {
                                if(element.roomName === roomName) {
                                    element.isSafe = true;
                                }
                            });
                            clearInterval(this.rooms[roomInfo[1]].endingTimer)
                            setTimeout(() => {
                                if(roomInfo[2].length >= 5) {
                                    this.rooms[roomInfo[1]].state = true;
                                    this.rooms[roomInfo[1]].round = 1;
                                    this.rooms[roomInfo[1]].remainingTime = 60;
                                    this.players.forEach(element => {
                                        if(element.roomName === roomName) {
                                            element.posX = 30;
                                            element.posY = canvas.height / 2;
                                            element.isSafe = false;
                                            element.isHunter = false;
                                        }
                                    });
                                    let bingo = getRandomInt(roomInfo[2].length);
                                    let roomPlayers = [];
                                    this.players.forEach(element => {
                                        if(element.roomName === this.rooms[roomInfo[1]].name) roomPlayers.push(element);
                                    });
                                    roomPlayers[bingo].isHunter = true;
                                    roomPlayers[bingo].posX = canvas.width / 2;
                                    this.rooms[roomInfo[1]].endingTimer = setInterval(() => {
                                        if(typeof this.rooms[roomInfo[1]] != 'undefined' && this.rooms[roomInfo[1]].remainingTime !== null) {
                                            if(this.rooms[roomInfo[1]].remainingTime > 0) {
                                                this.rooms[roomInfo[1]].remainingTime--;
                                            } else {
                                                this.rooms[roomInfo[1]].remainingTime = 60;
                                                this.handleRoom(this.rooms[roomInfo[1]].name, true);
                                            }
                                        }
                                    }, 1000);
                                }
                            }, 10000);
                            console.log('Partie terminé')
                        }
                    } else {
                        if(isSafe === huntedCount && this.rooms[roomInfo[1]].state) {
                            this.rooms[roomInfo[1]].round++;
                            this.rooms[roomInfo[1]].remainingTime = 60;
                            this.players.forEach(element => {
                                if(element.roomName === roomName) {
                                    if(element.isHunter) {
                                        element.posX = canvas.width / 2;
                                        element.posY = canvas.height / 2;
                                    } else {
                                        element.posX = 30;
                                        element.posY = canvas.height / 2;
                                        element.isSafe = false;
                                    }
                                }
                            });
                            clearInterval(this.rooms[roomInfo[1]].endingTimer)
                            this.rooms[roomInfo[1]].endingTimer = setInterval(() => {
                                if(typeof this.rooms[roomInfo[1]] != 'undefined' && this.rooms[roomInfo[1]].remainingTime !== null) {
                                    if(this.rooms[roomInfo[1]].remainingTime > 0) {
                                        this.rooms[roomInfo[1]].remainingTime--;
                                    } else {
                                        this.rooms[roomInfo[1]].remainingTime = 60;
                                        this.handleRoom(this.rooms[roomInfo[1]].name, true);
                                    }
                                }
                            }, 1000);
                            console.log('Manche suivante')
                        }
                    }
                } else {
                    if(this.rooms[roomInfo[1]].state) {
                        this.rooms[roomInfo[1]].round++;
                        this.rooms[roomInfo[1]].remainingTime = 60;
                        this.players.forEach(element => {
                            if(element.roomName === roomName) {
                                if(element.isHunter) {
                                    element.posX = canvas.width / 2;
                                    element.posY = canvas.height / 2;
                                } else {
                                    if(element.isSafe) {
                                        element.posX = 30;
                                        element.posY = canvas.height / 2;
                                        element.isSafe = false;
                                    } else {
                                        element.isHunter = true;
                                        element.posX = canvas.width / 2;
                                        element.posY = canvas.height / 2;
                                    }
                                }
                            }
                        });
                        console.log('Manche suivante')
                    }
                }
            }
        });
    }

    didCollide(direction, playerExist) {
        let state = "none"; 
        // console.log(this.players[playerExist[1]].posX);
        if(direction == "haut") {
            if((this.players[playerExist[1]].posY - this.players[playerExist[1]].radius) <= 0) state = 'wall';
        }
        else if(direction == "bas") {
            if((this.players[playerExist[1]].posY + this.players[playerExist[1]].radius) >= canvas.height) state = 'wall';
        }
        else if(direction == "gauche") {
            if((this.players[playerExist[1]].posX - this.players[playerExist[1]].radius) <= 0) state = 'wall';
        }
        else if(direction == "droite") {
            if((this.players[playerExist[1]].posX + this.players[playerExist[1]].radius) >= canvas.width) state = 'wall';
        }

        if(this.players[playerExist[1]].isHunter) {
            if(direction == "gauche") {
                if((this.players[playerExist[1]].posX - this.players[playerExist[1]].radius) <= 50) state = 'wall';
            }
            else if(direction == "droite") {
                if((this.players[playerExist[1]].posX + this.players[playerExist[1]].radius) >= (canvas.width - 50)) state = 'wall';
            }
        } else {
            if(direction == "droite") {
                if((this.players[playerExist[1]].posX - this.players[playerExist[1]].radius) >= (canvas.width - 50)) {
                    this.players[playerExist[1]].isSafe = true
                    this.players[playerExist[1]].posX = canvas.width - 30;
                    this.players[playerExist[1]].posY = canvas.height / 2;
                    this.io.to(this.players[playerExist[1]].roomName).emit('playerSafe', {});
                }
            }
        }

        this.players.forEach(element => {
            if(element.socket.id !== this.players[playerExist[1]].socket.id && element.roomName === this.players[playerExist[1]].roomName) {
                // console.log(`other : ${element.posY-element.radius} to ${element.posY+element.radius}`)
                if(
                (((element.posY - element.radius) <= (this.players[playerExist[1]].posY - this.players[playerExist[1]].radius) && (this.players[playerExist[1]].posY - this.players[playerExist[1]].radius) <= (element.posY + element.radius)) 
                || 
                ((element.posY - element.radius) <= (this.players[playerExist[1]].posY + this.players[playerExist[1]].radius) && (this.players[playerExist[1]].posY + this.players[playerExist[1]].radius) <= (element.posY + element.radius))) 
                
                && 
                
                (((element.posX - element.radius) <= (this.players[playerExist[1]].posX - this.players[playerExist[1]].radius) && (this.players[playerExist[1]].posX - this.players[playerExist[1]].radius) <= (element.posX + element.radius)) 
                || 
                ((element.posX - element.radius) <= (this.players[playerExist[1]].posX + this.players[playerExist[1]].radius) && (this.players[playerExist[1]].posX + this.players[playerExist[1]].radius) <= (element.posX + element.radius))) 
                
                ) {
                    if(element.isHunter && this.players[playerExist[1]].isHunter === false) {
                        this.io.to(element.roomName).emit('playerHit', {});
                        this.players[playerExist[1]].isHunter = true;
                    }
                    if(this.players[playerExist[1]].isHunter && element.isHunter === false) {
                        this.io.to(element.roomName).emit('playerHit', {});
                        element.isHunter = true;
                    }
                }
            }
        });

        return state;
    }
  
    run() {
        this.io.on('connection', (socket) => {
                    socketInterval[socket] = setInterval(() => {
                        let playerExist = this.playerExist(socket);
                        if(playerExist[0]) {
                            let handleData = [];
                            this.players.forEach(element => {
                                if(element.roomName === this.players[playerExist[1]].roomName) handleData.push({'socket': element.socket.id, 'name': element.name, 'isSafe': element.isSafe, 'bonus': element.bonus, 'isHunter': element.isHunter, 'posX': element.posX, 'posY': element.posY, 'radius': element.radius})
                            });
                            socket.emit('handlePlayersState', { 
                                players: handleData
                            });
                            if(typeof this.players[playerExist[1]] == 'undefined') return; 

                            this.roomInfo(this.players[playerExist[1]].roomName).then(roomInfo => {
                                if(roomInfo[0]) {
                                    socket.emit('handleRoomState', { 
                                        state: this.rooms[roomInfo[1]].state,
                                        round: this.rooms[roomInfo[1]].round,
                                        remainingTime: this.rooms[roomInfo[1]].remainingTime
                                    });
                                }
                            });

                            this.handleRoom(this.players[playerExist[1]].roomName);

                            let bingo = getRandomInt(10000);
                            if(bingo === 500) {
                                let bonus = getRandomInt(1);
                                this.players[playerExist[1]].bonus = bonus;
                            }
                        }
                    }, 5);
            
            socket.on('joinRoom', ({name, roomName, userId}) => {
                this.createRoom(roomName, false);
                this.createPlayer(socket, name, roomName, userId);
                socket.join(roomName);
                let playerExist = this.playerExist(socket); 
                this.roomInfo(roomName).then(roomInfo => {
                    if(roomInfo[0] && playerExist[0]) {
                        if(this.rooms[roomInfo[1]].state) {
                            clearInterval(socketInterval[socket])
                            this.players.splice(playerExist[1], 1);
                            roomInfo[2].length--;
                            socket.disconnect(true)
                            return;
                        }
                        this.players.forEach(element => {
                            if(element.name === name && element.socket.id !== socket.id && userId !== 0) {
                                clearInterval(socketInterval[socket])
                                this.players.splice(playerExist[1], 1);
                                roomInfo[2].length--;
                                socket.disconnect(true)
                                return;
                            }
                        });
                        console.log('NB JOUEUR : ' + roomInfo[2].length);
                        if(roomInfo[2].length === 5) {
                            this.players.forEach(element => {
                                if(element.roomName === roomName) {
                                    element.posX = 30;
                                    element.posY = canvas.height / 2;
                                    element.isSafe = false;
                                    element.isHunter = false;
                                }
                            });
                            let bingo = getRandomInt(roomInfo[2].length);
                            let roomPlayers = [];
                            this.players.forEach(element => {
                                if(element.roomName === this.rooms[roomInfo[1]].name) roomPlayers.push(element);
                            });
                            roomPlayers[bingo].isHunter = true;
                            roomPlayers[bingo].posX = canvas.width / 2;
                            this.rooms[roomInfo[1]].state = true;
                            this.rooms[roomInfo[1]].round = 1;
                            this.rooms[roomInfo[1]].remainingTime = 60;
                            this.rooms[roomInfo[1]].endingTimer = setInterval(() => {
                                if(typeof this.rooms[roomInfo[1]] != 'undefined' && this.rooms[roomInfo[1]].remainingTime !== null) {
                                    if(this.rooms[roomInfo[1]].remainingTime > 0) {
                                        this.rooms[roomInfo[1]].remainingTime--;
                                    } else {
                                        this.rooms[roomInfo[1]].remainingTime = 60;
                                        this.handleRoom(this.rooms[roomInfo[1]].name, true);
                                    }
                                }
                            }, 1000);
                        }
                        // console.log(`State : ${this.rooms[roomInfo[1]].state} et count : ${roomInfo[2]}`);
                    }
                });


                console.log(`Connnect Players (${this.players.length}) Rooms (${this.rooms.length})`);
            });

            socket.on('move', ({direction}) => {
                let playerExist = this.playerExist(socket);
                if(playerExist[0]) {
                    this.roomInfo(this.players[playerExist[1]].roomName).then(roomInfo => {
                        if(roomInfo[0] && this.rooms[roomInfo[1]].state && !this.players[playerExist[1]].isSafe) {
                            switch (direction) {
                                case 'haut': 
                                    if(this.didCollide(direction, playerExist) === "none") this.players[playerExist[1]].posY -= this.players[playerExist[1]].speed;
                                    break;
                                case 'bas':
                                    if(this.didCollide(direction, playerExist) === "none") this.players[playerExist[1]].posY += this.players[playerExist[1]].speed;
                                    break;
                                case 'gauche':
                                    if(this.didCollide(direction, playerExist) === "none") this.players[playerExist[1]].posX -= this.players[playerExist[1]].speed;
                                    break;
                                case 'droite':
                                    if(this.didCollide(direction, playerExist) === "none") this.players[playerExist[1]].posX += this.players[playerExist[1]].speed;
                                    break;
                                default:
                                    console.log(`Error`);
                                    break;
                            }
                        }
                    });
                }  
            });

            socket.on('bonus', () => {
                let playerExist = this.playerExist(socket);
                if(playerExist[0]) {
                    this.handleBonus(playerExist);
                }
            });

            socket.on('message', ({message}) => {
                let playerExist = this.playerExist(socket);
                if(playerExist[0]) {
                    let playerName = this.players[playerExist[1]].name;
                    let roomName = this.players[playerExist[1]].roomName;
                    this.io.to(roomName).emit('message', {playerName : playerName, message : message});
                }
            });

            socket.on('disconnect', () => {
                let playerExist = this.playerExist(socket); 
                if(playerExist[0]) {
                    let roomName = this.players[playerExist[1]].roomName;
                    clearInterval(socketInterval[socket])
                    this.players.splice(playerExist[1], 1);

                    this.roomInfo(roomName).then(roomInfo => {
                        if(roomInfo[0]) {
                            if(roomInfo[2].length === 0) {
                                this.rooms.splice(roomInfo[1], 1);
                                console.log(`Disconnect Players (${this.players.length}) Rooms (${this.rooms.length})`);
                            }
                        }
                    });

                }
            });

        });
    }
}

export {Game};