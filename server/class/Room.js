class Room {
    constructor(name, state) {
        this.name = name
        this.state = state;
        this.round = 0;
        this.endingTimer = null;
        this.remainingTime = 60;
    }
}

export {Room};