const canvas = {
    'height': 500,
    'width': 800, 
}

class Player {
    constructor(socket, name, roomName, userId) {
        this.socket = socket;
        this.userId = userId;
        this.name = name;
        this.roomName = roomName;
        this.posX = 30;
        this.posY = canvas.height / 2;
        this.speed = 1;
        this.radius = 15;
        this.isHunter = false;
        this.kills = 0;
        this.bonus = null;
    }
}

export {Player};