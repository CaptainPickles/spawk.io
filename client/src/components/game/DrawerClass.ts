export class Drawer {
    ctx : CanvasRenderingContext2D;
    canvas:HTMLCanvasElement;

    constructor(ctx: CanvasRenderingContext2D , canvas:HTMLCanvasElement) {
        this.ctx = ctx;
        this.canvas = canvas
      }

      //TODO : call un event au moment ou un joueur rejoins la game pour voir l etat de la game
      //Creer un ligne d arriver et une ligne de depart
      //creer une camera qui suis uniquement son joueur

    setPolice(){
        this.ctx.font = '20px Helvetica';
        this.ctx.textAlign = 'center';
        this.ctx.textBaseline = 'top';
    }

    drawHunted(x:number,y:number,radius:number ){    
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = 'green';
        this.ctx.fill();
    }
    drawHunter(x:number,y:number,radius:number,pseudo : string ){    
        this.ctx.fillStyle = "black";
        this.ctx.font = `${radius}px Helvetica`;
        this.ctx.fillText(pseudo, x , y -(radius*2.5));
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = 'black';
        this.ctx.fill();
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius - 2, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = 'red';
        this.ctx.fill();
    }
    drawPlayer(x:number,y:number,radius:number,pseudo : string,isHunter: boolean,isSafe:boolean,bonus:null|number ){    
        if(isSafe){
            return 
        }
        this.ctx.fillStyle = "black";
        this.ctx.font = `${radius}px Helvetica`;
        this.ctx.fillText(pseudo, x , y -(radius*2.5));
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
        if(bonus === null){
            this.ctx.fillStyle = 'black';
        }else {
            this.ctx.fillStyle = 'orange';
        }
        this.ctx.fill();
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius - 2, 0, 2 * Math.PI, false);
        let color =''
        isHunter ? color="red" : color="green"
        this.ctx.fillStyle = color;
        this.ctx.fill();
    }
    clearCanvas(clearAll? : boolean){
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        if(clearAll === true)return
        this.drawLimit()
    }
    drawLimit(){
    this.ctx.beginPath();
    this.ctx.moveTo(50, 0);
    this.ctx.lineTo(50, this.canvas.height);
    this.ctx.stroke();
    this.ctx.beginPath();
    this.ctx.moveTo(this.canvas.width-50, 0);
    this.ctx.lineTo(this.canvas.width-50, this.canvas.height);
    this.ctx.stroke();
    }
    drawWaitingScreen(status :string){
        if(status === "En attente"){
            const image = new window.Image();
            image.src = "https://zupimages.net/up/21/24/y51c.png";
            const self = this
            image.onload = () => {
              // set image only when it is loaded
              self.ctx.drawImage(image,self.canvas.width/3,0,250,200)
            };
        }
        if(status === "Partie terminé"){
            this.ctx.fillStyle = "black";
            this.ctx.font = `30px Helvetica`;
            this.ctx.fillText("La partie va se relancer", this.canvas.width/2 ,this.canvas.height/1.5);
        }
        this.ctx.fillStyle = "black";
        this.ctx.font = `50px Helvetica`;
        this.ctx.fillText(status, this.canvas.width/2 ,this.canvas.height/2);
    }

    drawTimer(time:string){
        this.ctx.fillStyle = "black";
        this.ctx.font = `20px Helvetica`;
        this.ctx.fillText(time, this.canvas.width/2 , 0);
    }

}


