export abstract class AudioClass {    


    public static playAudioVictory() {
        let audioVictory = new Audio(require('@/assets/sounds/Victory1.wav'))
        audioVictory.play()
    }
    public static playAudioHit(){
        let audioVictory = new Audio(require('@/assets/sounds/hit.wav'))
        audioVictory.play()
    }
    public static playAudioFinish(){
        let audioVictory = new Audio(require('@/assets/sounds/finish.wav'))
        audioVictory.play()
    }
}