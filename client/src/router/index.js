import Vue from 'vue'
import VueRouter from 'vue-router'
import GamePage from '../views/GamePage.vue'
import GameConnexionPage from '../views/GameConnexionPage.vue'
import About from '../views/AboutPage.vue'
import Ranking from '../views/RankingPage.vue'
import Connexion from '../views/ConnexionPage.vue'
import Inscription from '../views/InscriptionPage.vue'
import Account from '../views/AccountPage.vue'
import { getCookie } from '../utils/cookie-typescript-utils';

import store from '../store/index';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'GameConnexion',
    component: GameConnexionPage
  },
  {
    path: '/jeu',
    name: 'Game',
    component: GamePage,
    //verifie dans le store si un pseudo et une salle on été selectionné si non on refuse l acces au composant
    beforeEnter: (to, from, next) => {
      if(store.getters.getValidityGame  == false) {
          next(false);
      } else {
          next();
      }
  }
},
  {
    path: '/a-propos',
    name: 'About',
    component: About
  },
  {
    path: '/classement',
    name: 'ranking',
    component: Ranking
  },  {
    path: '/connexion',
    name: 'connexion',
    component: Connexion,
       //si le coockie userId n'est pas set si il ne n'est pas on refuse l'accés
    beforeEnter: (to, from, next) => {
      if(getCookie("userId")  !== undefined) {
          next(false);
      } else {
          next();
      }
  }
  },  {
    path: '/inscription',
    name: 'inscription',
    component: Inscription,
        //si le coockie userId n'est pas set si il ne n'est pas on refuse l'accés
        beforeEnter: (to, from, next) => {
          if(getCookie("userId")  !== undefined) {
              next(false);
          } else {
              next();
          }
      }
  },  {
    path: '/mon-compte',
    name: 'accout',
    component: Account,
            //si coockie userId est undefined on refuse l acces
            beforeEnter: (to, from, next) => {
              if(getCookie("userId")  === undefined) {
                  next(false);
              } else {
                  next();
              }
          }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
