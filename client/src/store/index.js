import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    playerName: '',
    roomName: '',
    socket: {}
  },
  mutations: {
    ChangePlayerName(state,payload){
      state.playerName = payload.newPlayerName
    },
    ChangeRoomName(state,payload){
      state.roomName = payload.newRoomName
    },
    ChangeSocket(state,payload){
      state.socket = payload
    }
  },
  getters:{
    getPlayerName(state){
      return state.playerName
    },
    getRoomName(state){
      return state.roomName
    },
    getSocket(state){
      return state.socket
    },
    //bolean return true if room name is not false 
    getValidityGame(state){
      if(state.roomName != '') return true
      return false 
    }
  },
  actions: {
    setPlayerName(context,payload){
      context.commit('ChangePlayerName', payload)
    },
    setRoomName(context,payload){
      context.commit('ChangeRoomName', payload)
    },    setSocket(context,payload){
      context.commit('ChangeSocket', payload)
    }
  },
  modules: {
  }
})
